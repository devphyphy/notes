/*----------------------------------------------------------
側欄　往左收合
-----------------------------------------------------------*/

$('.hamburger').click(function(){
	$(this).toggleClass('is--active');

	$('.sidebar').toggleClass('is--open');
	$('.form__group').toggleClass('is--active');
	$('.sidebar__profile').toggleClass('is--active');
	$('.sidebar__nav').toggleClass('is--active');
})