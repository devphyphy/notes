# http_path = "/"
http_path = ''
css_dir = "public/css"
sass_dir = "src/sass"
images_dir = "../images"
javascripts_dir = "js"

# 新增source maps設定
sourcemap = true

# 編譯註解型態 :expanded or :nested or :compact or :compressed
output_style = :expanded

# 相對路徑
 relative_assets = true

# 是否編譯註解
 line_comments = false


# preferred_syntax = :sass

