const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    devtool:'source-map',
    entry: {
        index: [
            // "webpack-hot-middleware/client?reload=true",
            "./src/index.js"
        ]
    },
    mode: "development",
    output: {
        // sourceMapFilename: '[name].map?[contenthash]',
        filename: "[name]-bundle.js",
        path: path.resolve(__dirname, "./public")
    },
    devServer: {
        contentBase: "public",
        overlay: true,
        hot: true,
        // 自己加
        host: '127.0.0.1',
        port: 3000,
        inline: true,  // 自動刷新頁面
        historyApiFallback: false, // 設置為true，所有的跳轉將指向index.html
        stats: {
            colors: true
        }
    },
    module: {
        rules: [
            {
                test:/\.js$/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                'react',
                                'stage-0',
                                'es2015',
                                ['env', { targets: {browsers: ['last 2 versions'] }}]
                            ],
                            "plugins": [
                                "transform-runtime",
                                "transform-class-properties"
                            ]
                        }
                    }
                ],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: MiniCssExtractPlugin.loader },
                    { loader: "css-loader" }
                ]
            },
            {
                test: /\.s(a|c)ss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: MiniCssExtractPlugin.loader },
                    { 
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                        // query: {
                        //     modules: true,
                        //     localIdentName: "[name]--[local]--[hash:base64:8]"
                        // } 
                    },
                    { 
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [require('autoprefixer')({
                                'browsers': ['> 1%', 'last 2 versions']
                            })],
                            sourceMap: true
                        }
                    },
                    { loader: "sass-loader?sourceMap",
                        options: {
                          "includePaths": [
                            path.resolve(__dirname, 'node_modules'),
                            path.resolve(__dirname, 'src')
                          ]
                        }
                     }
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: {
                            attrs: ["img:src"]
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|gif|png)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "images/[name]-[hash:8].[ext]"
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: "./src/index.html"
        }),
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            // filename: "[name]-[contenthash].css"
            filename: "[name].css"
            // 輸出index.css,對應entry的key值
        })
    ]
}